const rules = require('./webpack.rules');
const plugins = require('./webpack.plugins');

rules.push({
  test: /\.css$/,
  use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
},
{test: /\.(png|jpe?g|gif|ico|svg)$/, // We will handle of these file extensions
use: [
  {
    loader: "file-loader",
  }
]},
// {test: /\.svg/,
// use: [
//   {
//     loader: 'svg-url-loader',
//     // options: {
//     //   limit: 10000,
//     // },
//   },
// ],
// }
);

module.exports = {
  module: {
    rules,
  },
  plugins: plugins,
  resolve: {
    extensions: ['.js', '.ts', '.jsx', '.tsx', '.css', '.svg'],
  },
};
