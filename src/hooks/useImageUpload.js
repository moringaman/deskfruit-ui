
import S3 from 'react-aws-s3';
import { useState } from 'react'
window.Buffer = window.Buffer || require("buffer").Buffer;

const useImageUpload = (props) => {

    const S3_BUCKET ='deskfruit';
    const DIR = 'users'
    const REGION ='eu-west-2';
    const ACCESS_KEY = 'AKIAZES2ID2EYYJLTC7Q';
    const SECRET_ACCESS_KEY = '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';


    const config = {
        bucketName: S3_BUCKET,
         dirName: DIR,
        region: REGION,
        accessKeyId: ACCESS_KEY,
        secretAccessKey: SECRET_ACCESS_KEY,
    }

    const [fileData, setFileData] = useState(null)

    const uploadToS3 = async(file) => {

        const ReactS3Client = new S3(config);

        ReactS3Client
        .uploadFile(file)
        .then(data => {
            console.log(data)
            setFileData(data)
        })
        .catch(err => console.error(err))
    }

    return {
        uploadToS3,
        fileData
    }  
}

export default useImageUpload;