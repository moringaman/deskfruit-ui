import React, {useEffect, useState, memo } from 'react'
import Toggle  from 'rsuite/Toggle'
import chevrons from '../images/chevrons.png'
import loadImage from '../helpers/imageLoader'
import { generateFromString } from 'generate-avatar'
import  'rsuite/dist/rsuite.min.css'

const UserCard = (props: any) => {
    const { selected, newuser, data, clickevent, id, enableuser, enabled} = props
    console.log(data)
 
  return (
       <div
        onClick={() => clickevent(id)} 
        className={
            `card-user
             ${selected ? 'bordered' : null}
             ${newuser ? 'card-user_new' : null}`
            }
        >
         {
         newuser &&
            <div id="cross"></div>
          } 
       <div 
          className={`txt-sm`}
          style={{ paddingTop: '10px', paddingBottom: '5px'}}
        >
            {data && data.name}
        </div>
       <div className={`avatar `}>
       { !newuser && 
          <img alt='X' 
          src={data.image && data.image }
          />
       }
       </div>
       {
         !newuser &&
         <Toggle 
            loading={false} 
            size="sm"
            checked={enabled === id} 
            onChange={() => {
              if(enabled === id) {
                enableuser(0)
                return
              }
              enableuser(id)
            }}
         style={{margin: 'auto', width: '40px', marginTop: '5px', marginBottom: '5px'}}
         {...props} 
    />
       }
       
       {
       selected && 
       <div className='chevrons'>
       <img alt='^' src={chevrons} className='chevron' />
       </div>
       }
       </div>
  )
}

export default memo(UserCard)