
import React from 'react';

const Header = () => {
const screenName = "Desk users"
    return (
        <>
        <div className="header-container">
            <div className="logo-container">
               <div className="logo-container-inner">
                     desk<span>fruit</span>
               </div>
            </div>
            <div className={`dot-sml primary`} id="header-dot"></div>
            <div id="screen-name" className="txt-md">{screenName}</div>
            <div className="dot-lrg" id="header-dot-lrg">
                <div className="txt-lrg_white">+</div>
            </div>
        </div>
        </>
    )
}

export default Header