import React, {useState, useEffect, useContext, useCallback } from 'react';
import { getDeskData } from '../store/actionCreators';
import { DeskContext } from '../store/store'


export default function Footer () {
    const [appState, appDispatch]:any = useContext(DeskContext);

    useEffect(() => {
        fetchDeskData()
    },[])

    const fetchDeskData = async() => {
       await getDeskData(appDispatch, '24003e000e51353338363333')
    }

    const { desk } = appState || {}
  return (
    <div id='footer'>
        <div>
            <span style={{fontWeight: 'bold'}}>Desk ID: </span>{desk && desk.id} 
        </div>
        <div>
            <span style={{fontWeight: 'bold'}}>Location: </span> { desk && desk.location}
        </div>
        <div>
        <span style={{fontWeight: 'bold'}}>Status: </span>{ desk && desk.status}
            <div 
                className={`dot-sml ${desk.status === 'Online' ? 'secondary' : 'warning'}`} 
                id="status-dot">
            </div>
        </div>
        <div>
            <span style={{fontWeight: 'bold'}}>Position: </span>Up
        </div>
         </div>
  )
}
