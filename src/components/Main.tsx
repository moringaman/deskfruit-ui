import React  from 'react';
import Header from './Header'
import Store from '../store/store'
import Users from './Users'
import CurrentUser from './CurrentUser'
import Footer from './Footer'
//import { users } from '../data/users'

const Main = () => {
   
    return (
        <>
        <Store>
                <Header />
                <div className="container">
                    <>
                    <Users 
                     // data={users}
                    />
                    <CurrentUser />
                    </> 
                </div>
                <Footer />
        </Store>
        </>
    )
}

export default Main