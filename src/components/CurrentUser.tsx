import React, { useContext, useEffect, useState, memo }from 'react'
import { DeskContext } from '../store/store'
import useImageUpload from '../hooks/useImageUpload'
import { updateAvatar, updateUserSchedule } from '../store/actionCreators'
import { User } from '../data/types'
import Schedule from './Schedule'
declare global {
  interface window {
    electron: any;
  }
}

const CurrentUser: React.FC = () => {

    const { uploadToS3, fileData } = useImageUpload()
    
    const [appState, appDispatch]:any = useContext(DeskContext);

     const [currentUser, setCurrentUser ] = useState<User>(null)
     const [ userImage, setUserImage ] = useState(null)
     const { standingHeight, seatedHeight } = currentUser || {}

     const { apiDesk}  = appState

     const updateSchedule = () => {
      
       updateUserSchedule(appDispatch, currentUser, apiDesk.expression, apiDesk)
      //  window.electron.onButtonClick(schedule)
      window.electron.startCronJob(apiDesk.expression)
     }

     useEffect(() => {
       if(appState.apiDesk?.users) {
        const current = appState.apiDesk.users.find((el:any) => el._id === appState.currentUser)
      //  console.log('CURRENT ___>',  current.image)

        setCurrentUser(current)
        current?.image && setUserImage(current.image)
       } 
     }, [appState])

     useEffect(() => {
      console.log("FileData ", fileData)
      //TODO Update apiDesk Users image url
      if(fileData?.location === undefined) return
      console.log('MUTATE STATE ', fileData.location, currentUser._id)
      updateAvatar(appDispatch, currentUser, fileData.location, appState.apiDesk)

     }, [fileData])

     const uploadAvatar = async(e:any) => {
      //  const { result } 
       await uploadToS3(e.target.files[0])
     }

     const newUserScreen = () => {
       return (
         <div>
           SetUp New User Details
           <button className="btn">Configure</button>
         </div>
       )
     }

  return (
      <>
       <div className={`${appState.mode !== 'edit' ? 'frame-current-user' : 'frame-current-user_edit'}`}>
         {
           appState.mode === 'default' ?
           <> 
           {  currentUser?._id && currentUser._id == '00000000000' ? 
           <>
              {newUserScreen()}
             </>:
          <div id='heights'>
             <div>
             Standing Height: {standingHeight !== undefined ? `${standingHeight} inch` : '__._'} 
             </div>
             <div>
            Seated Height: {seatedHeight !== undefined ? `${seatedHeight} inch` : '__._'} 
            </div>
            <button className="btn" onClick={() => appDispatch({type: 'MODE', value: 'edit'})}>
              <div className="txt_sml">Update Settings</div>
            </button>
            {currentUser && currentUser._id}
           </div>
           }

           </>
           :
           <>
           <div id="settings-icons">
             <div className="dot-lrg"></div>
            <div className={`avatar circle-lrg`} id="large-avatar">
                { userImage &&
                    <img alt='X' 
                    src={userImage}
                    />
                 }
            </div>
            <div className="dot-lrg">
              <label className="custom-file-upload">X
                <input type="file" onChange={(e) => uploadAvatar(e)}/>
              </label>
            </div>
           </div>
           <Schedule 
             user={currentUser}
          //  handleClick={updateSchedule}
           />
           <div id="button-row">
            <button className="btn" onClick={() => updateSchedule()}>
                <div className="txt_sml">Update Schedule</div>
              </button>
              <button className="btn" onClick={() => appDispatch({type: 'MODE', value: 'default'})}>
                <div className="txt_sml">back</div>
              </button>
           </div>
           </>
         }
 
       </div>
      </>
   
  )
}

export default memo(CurrentUser)