
import React, {useState, useEffect, useContext, useCallback } from 'react';
import { DeskContext } from '../store/store'
import UserCard from './UserCard'
import { User } from '../data/users'
import { getUsers, selectedUser, tmpUser, enableUser} from '../store/actionCreators'

const Users = (props: any) => {

    const [appState, appDispatch]:any = useContext(DeskContext);

    if (appState.mode === 'edit') return;

    // const lastUser = () => {
    //     console.log(":AST USER FUNC")
    //     const latestUser = localStorage.getItem('user')
    //     if(latestUser !== '') {
    //         let latest = appState.apiDesk.users.find((el:any) => (el._id === latestUser))
    //      console.log("latest ", latest )

    //         return latest[0]._id
    //     }
    //     const latest = appState.apiDesk.users.reduce((a:User,b:User) => (a.lastUsage > b.lastUsage ? a : b))
    //      console.log("latest ", latest )
    //     return latest[0]._id
    // }
   // const { data } = props

    useEffect(() => {
    //  setLastUser();
    //  fetchUsers()
    },[])

    useEffect(() => {
        console.log('LAST USER ==>', appState.apiDesk.user )
        if (appState.apiDesk?.users) { 
        setLastUser()  
        }
    }, [appState.apiDesk])

    const fetchUsers = async() => {
     await getUsers(appDispatch)
    }
 
    useEffect(() => {
        console.log("APP STATE --> ", appState)
    }, [appState])
    

    const setLastUser = useCallback(() => {
        const latestUser = localStorage.getItem('user') 
            // if (appState.apiDesk.currentUser !== undefined) {
            //     setUser(lastUser())
               appDispatch({type: 'CURRENT_USER', value: latestUser})
                // latestUser !== '' ? latestUser : appState.users.lastUser()._id})
            //   } 
    }, [])

    
    const [ user, setUser ] = useState(undefined)

    // console.log("lastUser ", lastUser())

    const memoizedSelectUser = useCallback((n:string) => selectUser(n), [appState.apiDesk])

    const selectUser = (n:string) => {
        console.log("OIOIOIOI ", n, appState.apiDesk.currentUser )
        if(n === appState.apiDesk.currentUser) return
        const userSelected = appState.apiDesk.users.filter((el:any) => (el._id === n))
        console.log("PPPPPPPPP ", userSelected[0])
        setUser(userSelected[0])
        localStorage.setItem('user', userSelected[0]._id)
        selectedUser(appDispatch, '24003e000e51353338363333', userSelected[0]._id)
        // appDispatch({type: 'CURRENT_USER', value: userSelected[0]._id})
        //TODO: POST - Current user to API to be called by proton

    }

    const addUser = () => {
        console.log("Adding new user")
        !appState.apiDesk.users.find((el:any) => (el._id === '00000000000')) ?
        tmpUser(appDispatch)
        : null
    }

    const enable = async(n:string) => {
       if(n === '00000000000') return
       console.log("ENABLE ", n)
         await enableUser(appDispatch, '62813ae7e49e0b57aca50661', n)
    }

    return (
        <>
         <div className="user-container">
        {
               appState.apiDesk?.users && appState.apiDesk.users.map((el:any, i:number) => 
                  <UserCard 
                    key={el._id} 
                    data={el} 
                    selected={appState.apiDesk.currentUser == el._id} 
                    enableuser={enable} 
                    enabled={appState.apiDesk.enabled} 
                    id={el._id}
                    clickevent={memoizedSelectUser} 
                    />
                )
            }
            {
               appState.apiDesk?.users && appState.apiDesk?.users.length < 4 && <UserCard newuser clickevent={addUser}/>
            }
      
        </div>
        </>
    )
}

export default Users