import { generateFromString } from 'generate-avatar'

const loadImage = async(data:any) => {
    try { 
      const image = await import(/* webpackMode: "eager" */ `../images/${data.image}`)
      console.log("IIIMAGE --> ", image)
      if(image === undefined) {
       return generateFromString(data.id)
      }
     return image.default
    }catch(err) {
      console.log("ERROR", err)
    }
  }

  export default loadImage