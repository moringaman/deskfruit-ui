import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Main from './components/Main'
import home from './images/home.png'

declare global {
  interface Window {
    electron: any;
  }
}

function render() {
  
  ReactDOM.render(
      <>
        <div className="blur"></div>
          <img alt="background" src={home} className="background"/>
        <Main />
      </>
  ,
   document.body
   );
}

render()