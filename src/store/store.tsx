import React, { createContext, useReducer } from "react";
import appReducer from "./appReducer";
import { users } from '../data/users'
import { User } from '../data/users'

export const DeskContext = createContext({});

 interface AppState {
    deskId: string,
    apiDesk: Desk | {},
    desk: Desk,
    users: User[],
    ApiUsers: User[],
    mode: string,
    enabled: number
}

interface Desk {
    id: string,
    location: string,
    status: 'Online' | 'Offline' | 'Connecting'
    currentUser: number,
    users: User[] | undefined
}

const initialState:AppState = {
  deskId: "24003e000e51353338363333",
  apiDesk: {},
  desk: {
      id: "24003e000e51353338363333",
      location: 'Head Office',
      status: 'Online',
      currentUser: null,
      users: undefined
  },
  ApiUsers: [],
  users: users,
  mode: 'default', // or default
  enabled: null
};



const Store = ({ children }:any) => {
  const [appState, appDispatch] = useReducer(appReducer, initialState);

  return (
    <DeskContext.Provider value={[appState, appDispatch]}>
      {children}
    </DeskContext.Provider>
  );
};

export default Store;
