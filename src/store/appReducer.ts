

const appReducer = (state:any, action:any) => {
    switch (action.type) {
        case "GET_DESK":
            return {
              ...state,
              apiDesk: action.value
            };
        case "GET_USERS":
            return {
              ...state,
              ApiUsers: action.value
            };
        case "SELECTED_USER":
            return {
                ...state,
                apiDesk: {
                    ...state.apiDesk,
                    currentUser: action.value
                }
            };
      case "ADD_USER":
          const newUser = action.value
        return {
          ...state,
          apiDesk: {
              ...state.apiDesk,
              users: [...state.apiDesk.users, newUser]
          }
        //   users: [...state.users.concat(action.value)]
        };
      case 'CURRENT_USER':
          return { 
              ...state,
              currentUser: action.value
          };
      case 'UPDATE_DESK_STATUS': 
          return { 
              ...state,
              desk: {
                  ...state.desk,
                  status: action.value
              }
          };
          case 'UPDATE_ACTIVE_SCHEDULE': 
          return { 
              ...state,
              apiDesk: {
                  ...state.apiDesk,
                  expression: action.value
              }
          }
          case 'UPDATE_SCHEDULE': 
          console.log(action.id, action.value)
          return { 
              ...state,
              apiDesk: {
                  ...state.apiDesk,
                  users: state.apiDesk.users.map(
                    (el:any) => el._id === action.id ? {...el, expression: action.value}
                    : el
                  )
              }
          }
          case 'UPDATE_AVATAR': 
          console.log(action.id, action.value)
          return { 
              ...state,
              apiDesk: {
                  ...state.apiDesk,
                  users: state.apiDesk.users.map(
                    (el:any) => el._id === action.id ? {...el, image: action.value}
                    : el
                  )
              }
          }
      case 'MODE':
          return { 
            ...state,
            mode: action.value
          }
        case 'ENABLE_USER':
          return { 
            ...state,
            apiDesk: {
              ...state.apiDesk,
              enabled: action.value
            }
          }
        case 'SHOW_FORM':
            return { 
                ...state,
                forms: action.value
            }
      default:
        return state;
    }
  };

  export default appReducer;