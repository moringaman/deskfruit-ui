import axios from "axios";
import { uid } from 'uid'
// Action Creators

export const getUsers = async(dispatch:any) => {
   
    var config = {
        method: 'get',
        url: 'http://localhost:3001/users',
        headers: {}
      };
    try {
        const response = await axios(config)
            console.log('AXIOS', response.data)
            dispatch({type: 'GET_USERS', value: response.data})
    }catch(err) {
        console.log(err)
    }
 }

 export const selectedUser = async(dispatch:any, id: string, userId:any) => {
    const data = JSON.stringify({
        currentUser: userId
    })
    var config = {
        method: 'patch',
        url: `http://localhost:3001/desks/${id}`,
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
      };
    try {
        const response = await axios(config)
            console.log('AXIOS', response.data)
            dispatch({type: 'SELECTED_USER', value: userId})
    }catch(err) {
        console.log(err)
    }
 }

 export const addUser = async(dispatch:any, user:any) => {

    var config = {
        method: 'post',
        url: `http://localhost:3001/users`,
        headers: {
            'Content-Type': 'application/json'
        }
      };
    try {
        const response = await axios(config)
            dispatch({type: 'ADD_USER', value: response.data})
    }catch(err) {
        console.log(err)
    }
 }

 export const getDeskData = async(dispatch:any, id: string) => {
   
    var config = {
        method: 'get',
        url: `http://localhost:3001/desks/${id}`,
        headers: {}
      };
    try {
        const response = await axios(config)
            // console.log('AXIOS', response.data)
            dispatch({type: 'GET_DESK', value: response.data})
    }catch(err) {
        console.log(err)
    }
 }

 export const saveDeskData = async(dispatch:any, id: string, desk: any) => {
   
   desk.users = desk.users.filter((user:any) => user._id !== '00000000000')

    var config = {
        method: 'patch',
        url: `http://localhost:3001/desks/${id}`,
        headers: {
            'Content-Type': 'application/json'
        },
        body: desk
      };
    try {
        const response = await axios(config)
            // console.log('AXIOS', response.data)
            dispatch({type: 'SAVE_DESK', value: response.data})
    }catch(err) {
        console.log(err)
    }
 }

export const tmpUser = (dispatch:any) => {

    const tempUser:any =  {
        _id: '00000000000',
        name: 'New User',
        image: 'https://avatars.dicebear.com/api/initials/N.svg',
        lastUsage: Date(),
        standingHeight: undefined,
        seatedHeight: undefined
    }

    dispatch({type: 'ADD_USER', value: tempUser})
    dispatch({type: 'SELECTED_USER', value: tempUser._id})
}

export const enableUser = async(dispatch: any, deskId:string, id: string) => {

    const data = JSON.stringify({
        "enabled": id
    })
    var config = {
        method: 'patch',
        url: `http://localhost:3001/desks/${deskId}`,
        headers: {
             'Content-Type': 'application/json'
        },
        data: data
      };
    try {
        await axios(config)
            // console.log('AXIOS', response.data)
            dispatch({type: 'ENABLE_USER', value: id})
    }catch(err) {
        console.log(err)
    }
}

export const updateAvatar = async(dispatch:any, user:any, image:string, desk:any) => {
    console.log('Mutating -->', desk.users)
    dispatch({type: 'UPDATE_AVATAR', value: image, id: user._id})

    const users = desk.users.map(
        (el:any) => el._id === user._id ? {...el, image: image}
        : el
      )

    const data = JSON.stringify({
       "users": users
    })
    console.log("DATA ", data)
    var config = {
        method: 'patch',
        url: `http://localhost:3001/desks/${desk.deskId}`,
        headers: {
             'Content-Type': 'application/json'
        },
        data: data
      };
    try {
        await axios(config)
    }catch(err) {
        console.log(err)
    }
}

export const updateUserSchedule = async(dispatch:any, user:any, schedule:string, desk:any) => {
    console.log('Mutating -->', desk.users)
    dispatch({type: 'UPDATE_SCHEDULE', value: schedule.replace(/['"']/g,""), id: user._id})

    const users = desk.users.map(
        (el:any) => el._id === user._id ? {...el, expression: schedule.replace(/['"']/g,"")}
        : el
      )

    const data = JSON.stringify({
       "users": users
    })
    console.log("DATA ", data)
    var config = {
        method: 'patch',
        url: `http://localhost:3001/desks/${desk.deskId}`,
        headers: {
             'Content-Type': 'application/json'
        },
        data: data
      };
    try {
        await axios(config)
    }catch(err) {
        console.log(err)
    }
}