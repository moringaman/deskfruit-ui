import fetch from 'node-fetch'

type Methods =  'POST' | 'PUT' | 'GET' | 'DELETE'

export const  apiCall = async(url:string, data:string | {}, method:Methods) => {
    var urlencoded = new URLSearchParams();

    let options:RequestInit | any = {
        method, // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer 7d057cfe36d51613cf71813b8f78f268754a4559'
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: `${method === 'POST' ? JSON.stringify(data) : null}` // body data type must match "Content-Type" header
      }

      if (method === 'POST') {
          options = {...options, body: urlencoded }//JSON.stringify(data)}
      }

      return await fetch(url, options)
}
