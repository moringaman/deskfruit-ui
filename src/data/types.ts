export type User = {
    _id: number | string,
    name: string,
    image: string,
    lastUsage: Date,
    standingHeight: number,
    seatedHeight: number,
    schedule: string
}

export type TimeOption = {
    value: string,
    label: string
}