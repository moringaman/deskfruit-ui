export type User = {
    id: number | string,
    name: string,
    image: string,
    lastUsage: Date,
    standingHeight: number,
    seatedHeight: number
}

// type users = Array<User>

export const users:User[] = [
    { 
        id: 1,
        name: 'Geoff',
        image: 'avatar_geoff.png',
        lastUsage: new Date("2017-01-26"),
        standingHeight: undefined,
        seatedHeight: undefined
    },
    // {
    //     id: 2,
    //     name: 'Edwuardo',
    //     image: 'avatar_edwuardo.png',
    //     lastUsage: new Date("2017-01-26"),
    //     standingHeight: 53.2,
    //     seatedHeight: 31.0
    // },
    {
        id: 2,
        name: 'Hope',
        image: 'avatar_hope.png',
        lastUsage: new Date('Sat May 07 2022 19:29:53 GMT+0100 (British Summer Time)'),
        standingHeight: undefined,
        seatedHeight: undefined
    },
];