const { ipcRenderer, contextBridge } = require("electron");

contextBridge.exposeInMainWorld('electron', {
    onButtonClick: async(data:any) => await ipcRenderer.send('saveContent', data),
    startCronJob: (schedule:String) => ipcRenderer.send('startCronJob', schedule),


    //   ipcRenderer: {
    //     // async onButtonClick(content:String) {
    //     //     return await ipcRenderer.send('saveContent', content)
    //     // },
    //     // startCronJob: (schedule:String) => ipcRenderer.send('startCronJob', schedule),
    //     onButtonClick: async() => await ipcRenderer.send('saveContent')
    //   }
  },
)